# MoneyTrack

Prototype for Pricount, a Tricount app clone that does not send data to third-parties.

Made with [MeteorJS](https://guide.meteor.com/).

## Install

You need to have meteor installed to run this project.

```
curl https://install.meteor.com/ | sh
```

Clone and cd into the repo and run:

```
meteor npm install
meteor
```

## Related projects

We refactored and updated this project in [https://gitlab.com/AugusteBerco/pricount-client](https://gitlab.com/AugusteBerco/pricount-client). This client is still under development.
