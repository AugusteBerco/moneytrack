module.exports = {
  extends: [
    "@meteorjs/eslint-config-meteor",
    "plugin:react/recommended",
    "prettier",
    "prettier/react",
  ],
  rules: {
    quotes: ["error", "double"],
    "import/extensions": ["off", "never"],
    "react/prop-types": ["off"],
    "react/jsx-wrap-multilines": [
      "error",
      { arrow: true, return: true, declaration: true },
    ],
    "react/jsx-props-no-spreading": [
      "error",
      {
        exceptions: ["LinearProgress"],
      },
    ],
  },
  settings: {
    "import/resolver": [
      {
        node: {
          extensions: [".js", ".jsx", ".ts", ".tsx"],
        },
      },
      {
        meteor: {
          moduleDirectory: ["imports"],
          extensions: [".js", ".jsx", ".ts", ".tsx"],
        },
      },
    ],
  },
};
