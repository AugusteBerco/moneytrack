import { Meteor } from "meteor/meteor";
import { check } from "meteor/check";
import ExpenseLists from "../imports/api/expenselists";
import Expenses from "../imports/api/expenses";
import methods from "../imports/api/methods";
import Participants from "/imports/api/participants";

Meteor.startup(() => {});

Meteor.publish("expenselists", function () {
  return ExpenseLists.find({}, {});
});

Meteor.publish("expenses", function (listId) {
  check(listId, String);

  return [
    ExpenseLists.find(listId),
    Expenses.find({ listId }),
    Participants.find({ listId }),
  ];
});

Meteor.publish("expensedetails", function (expenseId) {
  check(expenseId, String);

  const expense = Expenses.findOne(expenseId);

  if (!expense)
    throw new Meteor.Error("not-found", "The requested expense was not found");

  return [
    Expenses.find({ _id: expenseId }),
    Participants.find({ listId: expense.listId }),
  ];
});
