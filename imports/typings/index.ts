import currency from "currency.js";

export type Participant = {
  _id: string;
  name: string;
  userId: string;
};

export type Balance = {
  name: string;
  total: currency;
};

export type Transfer = {
  name: string;
  to: string;
  amount: currency;
};

export type Expense = {
  _id: string;
  title: string;
  amount: currency | number;
  paidBy: string;
  forWhom: string[];
  date: Date;
};

export type ExpenseList = {
  _id: string;
  title: string;
  expenses: () => Expense[];
  participants: () => Participant[];
  totalExpenses: () => currency;
  totalFor: (userId: string) => currency;
  balances: () => Balance[];
  transfers: () => Transfer[];
};
