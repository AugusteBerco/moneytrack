import { Grid } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import { Expense, Participant } from "/imports/typings";

interface IProps {
  expense: Expense;
  participants: Participant[];
}
const ExpenseListItem: React.FC<IProps> = ({ expense, participants }) => {
  const history = useHistory();
  const match = useRouteMatch();
  const paidByParticipant = participants.find((p) => p._id === expense.paidBy);

  const onListItemClick = () =>
    history.push(`${match.url}/expense/${expense._id}`);
  return (
    <>
      <ListItem onClick={onListItemClick}>
        <Grid container>
          <Grid container item xs={12}>
            <Grid item xs={6}>
              <Typography variant="h6">{expense.title}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6">{expense.amount}€</Typography>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid item xs={6}>
              <Typography variant="body1">
                paid by{" "}
                {paidByParticipant
                  ? paidByParticipant.name
                  : "Participant not found"}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </ListItem>
      <Divider />
    </>
  );
};

export default ExpenseListItem;
