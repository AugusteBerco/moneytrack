import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import React from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import ExpenseLists from "/imports/api/expenselists";
import Loading from "/imports/ui/components/Loading";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

const ExpensesListsList = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const classes = useStyles();
  const { expenses, loading, count } = useTracker(() => {
    const sub = Meteor.subscribe("expenselists");
    const lists = ExpenseLists.find();
    return {
      loading: !sub.ready(),
      expenses: lists.fetch(),
      count: lists.count(),
    };
  }, []);

  const handleListClick = (listId: string) => {
    history.push(`${match.url}/${listId}`);
  };

  if (loading) {
    return <Loading />;
  }
  if (count) {
    return (
      <div className={classes.root}>
        <List component="nav">
          {expenses.map((expense) => (
            <ListItem
              key={expense._id}
              onClick={() => handleListClick(expense._id)}
              button
            >
              {expense.title}
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
  return <p>No expenses yet</p>;
};

export default ExpensesListsList;
