import Box from "@material-ui/core/Box";
import Fab from "@material-ui/core/Fab";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import React from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import useAccount from "/imports/hooks/useAccount";
import { ExpenseList } from "/imports/typings";

const useStyles = makeStyles((theme) => ({
  footer: {
    // marginTop: "auto",
    width: "100%",
    padding: theme.spacing(3, 2),
    position: "fixed",
    bottom: 0,
    display: "flex",
    flexDirection: "row",
    backgroundColor: theme.palette.background.paper,
  },
  footerItem: {
    flexGrow: 1,
  },
}));

interface IProps {
  expenseList: ExpenseList;
}

const ExpensesListFooter: React.FC<IProps> = ({ expenseList }) => {
  const classes = useStyles();
  const history = useHistory();
  const match = useRouteMatch();
  const { userId } = useAccount();
  return (
    <Box className={classes.footer}>
      <div style={{ position: "absolute", top: "-20px", left: "50%" }}>
        <Fab
          size="small"
          color="primary"
          onClick={() => history.push(`${match.url}/add-expense`)}
        >
          <AddIcon />
        </Fab>
      </div>

      <Box className={classes.footerItem}>
        MY TOTAL <br />
        {expenseList.totalFor(userId || "").format()}€
      </Box>
      <Box className={classes.footerItem} style={{ textAlign: "right" }}>
        TOTAL EXPENSES <br />
        {expenseList.totalExpenses().format()} €
      </Box>
    </Box>
  );
};

export default ExpensesListFooter;
