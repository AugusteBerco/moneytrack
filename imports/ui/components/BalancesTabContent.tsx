import List from "@material-ui/core/List";
import React from "react";
import { Balance, Transfer } from "/imports/typings";
import BalanceListItem from "/imports/ui/components/BalanceListItem";
import TransferListItem from "/imports/ui/components/TransferListItem";

interface IProps {
  balances: Balance[];
  transfers: Transfer[];
}
const BalancesTabContent: React.FC<IProps> = ({ balances, transfers }) => {
  const maxDebtAmount = Math.abs(balances[0].total.value);
  const maxOwnsAmount = Math.abs(balances[balances.length - 1].total.value);
  return (
    <div style={{ display: "flex", flexDirection: "column", flex: 1 }}>
      <div style={{ width: "100%" }}>
        <List>
          {balances.map((balance: Balance) => (
            <BalanceListItem
              key={balance.name}
              balance={balance}
              max={
                maxDebtAmount > maxOwnsAmount ? maxDebtAmount : maxOwnsAmount
              }
              // maxOwnsAmount={maxOwnsAmount}
            />
          ))}
        </List>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          padding: "4px 4px",
        }}
      >
        {transfers.map((transfer: Transfer, index: number) => (
          // eslint-disable-next-line react/no-array-index-key
          <TransferListItem key={index} transfer={transfer} />
        ))}
      </div>
    </div>
  );
};

export default BalancesTabContent;
