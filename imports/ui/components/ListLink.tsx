import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PropTypes from "prop-types";
import React from "react";
import { Link as RouterLink } from "react-router-dom";

interface IProps {
  icon?: React.ReactNode;
  primary?: boolean;
  to: string;
}

const ListItemLink: React.FC<IProps> = ({
  icon = undefined,
  primary = false,
  to,
}) => {
  const renderLink = React.useMemo(
    () =>
      // eslint-disable-next-line react/display-name
      React.forwardRef<HTMLLIElement, ListItemProps>((itemProps, ref) => (
        // eslint-disable-next-line react/jsx-props-no-spreading
        <RouterLink to={to} ref={ref} {...itemProps} />
      )),
    [to]
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
};

ListItemLink.propTypes = {
  icon: PropTypes.node,
  primary: PropTypes.bool,
  to: PropTypes.string.isRequired,
};

ListItemLink.defaultProps = {
  icon: undefined,
  primary: false,
};

export default ListItemLink;
