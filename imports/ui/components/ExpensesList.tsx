import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { Expense, ExpenseList, Participant } from "/imports/typings";
import ExpenseListItem from "/imports/ui/components/ExpenseListItem";

const useStyles = makeStyles(() => ({
  listContainer: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
  },
}));

interface IProps {
  expenseList: ExpenseList;
  expenses: Expense[] | undefined;
  participants: Participant[];
}

const ExpensesList: React.FC<IProps> = ({ expenses, participants }) => {
  const classes = useStyles();

  return (
    <div className={classes.listContainer}>
      <Container maxWidth="lg" style={{ flexGrow: 1 }}>
        <Grid container>
          <Grid item xs={12}>
            {expenses && expenses.length ? (
              <List style={{ width: "100%" }}>
                {expenses.map((expense: Expense) => (
                  <ExpenseListItem
                    key={expense._id}
                    expense={expense}
                    participants={participants}
                  />
                ))}
              </List>
            ) : (
              <p>No expenses yet</p>
            )}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default ExpensesList;
