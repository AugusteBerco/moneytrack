import React from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import useAccount from "/imports/hooks/useAccount";
import Loading from "/imports/ui/components/Loading";
import ExpenseListPage from "/imports/ui/pages/ExpenseLists/index";
import HomePage from "/imports/ui/pages/HomePage";
import ListForm from "/imports/ui/pages/ListForm";
import LoginPage from "/imports/ui/pages/Login";
import RegisterPage from "/imports/ui/pages/Register";

const theme = createMuiTheme({
  palette: {
    type: "dark",
  },
});

const ProtectedRoute = ({ children, ...rest }: { children: any }) => {
  const { user, loggingIn } = useAccount();

  if (loggingIn) {
    return <Loading />;
  }
  return (
    <Route
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
      render={
        ({ location }) =>
          user ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location },
              }}
            />
          )
        // eslint-disable-next-line react/jsx-curly-newline
      }
    />
  );
};

const App = () => (
  <ThemeProvider theme={theme}>
    <Router>
      <Switch>
        <Route path="/new">
          <ListForm />
        </Route>
        <Route path="/login">
          <LoginPage />
        </Route>
        <Route path="/register">
          <RegisterPage />
        </Route>
        <ProtectedRoute path="/expenses">
          <ExpenseListPage />
        </ProtectedRoute>
        <Route path="/">
          <HomePage />
        </Route>
      </Switch>
    </Router>
  </ThemeProvider>
);

export default App;
