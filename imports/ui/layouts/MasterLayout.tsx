import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  root: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
  },
  main: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(2),
  },
  footer: {
    padding: theme.spacing(3, 2),
    marginTop: "auto",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[800],
  },
}));

interface IProps {
  appBarContent?: React.ReactNode;
}

const MasterLayout: React.FC<IProps> = ({ children, appBarContent }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <ToastContainer />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        {appBarContent}
      </AppBar>
      <main>
        <>{children}</>
      </main>
    </div>
  );
};

export default MasterLayout;
