import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import UserMenu from "/imports/ui/components/UserMenu";
import MasterLayout from "/imports/ui/layouts/MasterLayout";

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBarSpacer: theme.mixins.toolbar,
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
}));

const Layout: React.FC<{}> = ({ children }) => {
  const classes = useStyles();

  const { user } = useTracker(
    () => ({
      user: Meteor.user(),
    }),
    []
  );

  return (
    <MasterLayout
      appBarContent={
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            MoneyTrack
          </Typography>
          {user ? (
            <UserMenu />
          ) : (
            <>
              <Button color="inherit" component={Link} to="/login">
                Login
              </Button>
              <Button color="inherit" component={Link} to="/register">
                Register
              </Button>
            </>
          )}
        </Toolbar>
      }
    >
      <main>
        {/* <div className={classes.appBarSpacer} /> */}
        <Container maxWidth="lg">
          <Grid container>
            <Grid item>
              <>{children}</>
            </Grid>
          </Grid>
        </Container>
      </main>
    </MasterLayout>
  );
};
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
