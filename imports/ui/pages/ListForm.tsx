import { makeStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { Meteor } from "meteor/meteor";
import React, { Fragment } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import { createExpenseList } from "/imports/api/methods";
import FormLayout from "/imports/ui/layouts/FormLayout";

type ListFormInputs = {
  title: string;
  ownerName: string;
  others?: Array<{ name: string }>;
};

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    "& > *": {
      margin: theme.spacing(1),
      // width: "25ch",
    },
  },
}));
interface IProps {
  list?: ListFormInputs;
}

const ListForm: React.FC<IProps> = ({ list }) => {
  const classes = useStyles();
  const history = useHistory();

  const defaultList = list
    ? { ...list }
    : { title: "", ownerName: Meteor.user()?.username, others: [{ name: "" }] };

  const { register, handleSubmit, errors, control } = useForm<ListFormInputs>({
    defaultValues: {
      ...defaultList,
    },
  });
  const { fields, remove, insert } = useFieldArray({
    control,
    name: "others",
  });

  const saveList = (d: ListFormInputs) => {
    createExpenseList.call(
      { title: d.title, others: d.others.map((o) => o.name) },
      (err: Meteor.Error) => {
        console.log(err);
        if (err) {
          toast.error(err.reason);
        } else {
          history.push("/");
        }
      }
    );
  };

  const handleAddPeople = (index: number) => {
    insert(index, { nom: "test" });
  };

  const handleRemovePeople = (index: number) => {
    remove(index);
  };

  return (
    <FormLayout title="New List" handleCheck={handleSubmit(saveList)}>
      <div className={classes.paper}>
        <form className={classes.form} noValidate>
          <TextField
            name="title"
            inputRef={register}
            variant="filled"
            label="Name of the list"
            fullWidth
            error={!!errors.title}
            helperText={errors.title?.message}
          />
          <TextField
            name="ownerName"
            inputRef={register}
            variant="filled"
            label="Your name"
            fullWidth
            error={!!errors.ownerName}
            helperText={errors.ownerName?.message}
          />
          <Typography variant="h5">Other Participants</Typography>
          <Grid container spacing={3}>
            {fields.map((person, index) => (
              <Fragment key={`person-${person.id}`}>
                <Grid item xs={9}>
                  <TextField
                    name={`others[${index}].name`}
                    inputRef={register()}
                    variant="filled"
                    label="Name"
                    fullWidth
                  />
                </Grid>
                <Grid item xs={3}>
                  <Button onClick={() => handleAddPeople(index)}>Add</Button>
                  {fields.length > 2 ? (
                    <Button onClick={() => handleRemovePeople(index)}>
                      Remove
                    </Button>
                  ) : null}
                </Grid>
              </Fragment>
            ))}
          </Grid>
        </form>
      </div>
    </FormLayout>
  );
};

export default ListForm;
