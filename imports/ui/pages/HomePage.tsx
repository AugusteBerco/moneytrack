import Typography from "@material-ui/core/Typography";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import useAccount from "/imports/hooks/useAccount";
import Layout from "/imports/ui/layouts/Layout";

const HomePage = () => {
  const history = useHistory();
  const { isLoggedIn } = useAccount();

  useEffect(() => {
    if (isLoggedIn) {
      history.push("/expenses");
    }
  }, [isLoggedIn]);

  return (
    <Layout>
      <Typography variant="h2">Welcome home my son</Typography>
    </Layout>
  );
};

export default HomePage;
