import React from "react";
import FormLayout from "/imports/ui/layouts/FormLayout";

const ExpenseEditPage = () => {
  console.log("expense page");

  return (
    <FormLayout
      title="Edit expense"
      handleCheck={() => console.log("handling check")}
    >
      <p>Expense form</p>
    </FormLayout>
  );
};

export default ExpenseEditPage;
