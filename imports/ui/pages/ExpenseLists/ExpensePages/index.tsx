import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import {
  Route,
  Switch,
  useHistory,
  useRouteMatch,
  useParams,
} from "react-router-dom";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import ExpenseEditPage from "./ExpenseEdit";
import ExpenseViewPage from "./ExpenseView";
import Expenses from "/imports/api/expenses";
import Participants from "/imports/api/participants";
import Loading from "/imports/ui/components/Loading";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MasterLayout from "/imports/ui/layouts/MasterLayout";

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));

const ExpensePage = () => {
  console.log("expense page");
  const { expenseId } = useParams<{
    expenseId: string;
  }>();
  const history = useHistory();
  const match = useRouteMatch();
  const classes = useStyles();

  // console.log(expenseListId);

  const { ready, expense, participants } = useTracker(() => {
    const sub = Meteor.subscribe("expensedetails", expenseId);
    const exp = Expenses.findOne(expenseId);
    return {
      ready: sub.ready(),
      expense: exp,
      participants: Participants.find({ listId: exp.listId }).fetch(),
    };
  });

  console.log({ expense, participants, ready });

  const handleBack = () => history.goBack();

  if (!ready) {
    return (
      <MasterLayout
        appBarContent={
          <Toolbar>
            <Typography variant="h6">MoneyTrack</Typography>
          </Toolbar>
        }
      >
        <Loading />
      </MasterLayout>
    );
  }

  return (
    <Switch>
      <Route path={`${match.url}/edit`}>
        <ExpenseEditPage />
      </Route>
      <Route path={`${match.url}`}>
        <ExpenseViewPage expense={expense} participants={participants} />
      </Route>
    </Switch>
  );
};

export default ExpensePage;
