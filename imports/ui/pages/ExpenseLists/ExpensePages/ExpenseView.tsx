import { Box } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import { format } from "date-fns";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import React from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import { Expense, Participant } from "/imports/typings";
import MasterLayout from "/imports/ui/layouts/MasterLayout";
import useAccount from "/imports/hooks/useAccount";

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
    marginTop: theme.spacing(4),
  },
  subtitle: {
    flexGrow: 1,
  },
  paidBy: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
  },
}));

interface IProps {
  expense: Expense;
  participants: Participant[];
}

const ExpenseViewPage: React.FC<IProps> = ({ expense, participants }) => {
  console.log("expense page");
  const history = useHistory();
  const match = useRouteMatch();
  const classes = useStyles();
  const { userId } = useAccount();

  const paidByParticipant = participants.find((p) => p._id === expense.paidBy);

  const userIsParticipant = participants.find((p) => p.userId === userId);

  const handleBack = () => history.goBack();

  return (
    <MasterLayout
      appBarContent={
        <>
          <Toolbar style={{ alignItems: "start" }}>
            <IconButton onClick={handleBack} color="inherit">
              <ArrowBackIcon />
            </IconButton>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                flexGrow: 1,
                alignItems: "center",
              }}
            >
              <Typography variant="h4" className={classes.title}>
                {expense.title}
              </Typography>
              <Typography variant="h5" className={classes.subtitle}>
                €{expense.amount}
              </Typography>
            </div>
            <Button
              variant="text"
              color="inherit"
              onClick={() => history.push(`${match.url}/edit`)}
            >
              EDIT
            </Button>
          </Toolbar>
          <Box className={classes.paidBy}>
            <Typography variant="h6">
              paid by{" "}
              {paidByParticipant
                ? paidByParticipant.name
                : "Participant not found"}
            </Typography>
            <Typography variant="h6">
              {format(expense.date, "dd/MM/yyyy")}
            </Typography>
          </Box>
        </>
      }
    >
      <Box>
        <Typography variant="subtitle1">
          For {expense.forWhom.length} participant{" "}
          {userIsParticipant ? ", including me" : null}
        </Typography>
      </Box>
      <List component="nav" aria-label="expense for">
        {expense.forWhom.map((p) => (
          <ListItem key={p}>
            <ListItemText
              primary={
                participants.find((participant) => participant._id === p)?.name
              }
            />
            <ListItemSecondaryAction>test</ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </MasterLayout>
  );
};

export default ExpenseViewPage;
