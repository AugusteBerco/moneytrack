import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormLabel from "@material-ui/core/FormLabel";
import { Meteor } from "meteor/meteor";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { assignToList } from "/imports/api/methods";
import FormLayout from "/imports/ui/layouts/FormLayout";

type Participant = {
  _id: string;
  userId?: string;
  name: string;
};

interface IProps {
  listId: string;
  participants: Participant[];
}

const ParticipantChooser: React.FC<IProps> = ({ listId, participants }) => {
  const { handleSubmit, control } = useForm();

  const submitParticipant = (d: any) => {
    console.log(d);
    assignToList.call(
      { participantId: Object.keys(d)[0] },
      (err: Meteor.Error) => {
        if (err) {
          toast.error(err.reason);
        }
      }
    );
  };

  return (
    <FormLayout title="Choose" handleCheck={handleSubmit(submitParticipant)}>
      <FormLabel component="legend">Who are you?</FormLabel>
      <FormGroup>
        {participants.map((participant: Participant) => (
          <FormControlLabel
            key={participant._id}
            control={
              <Controller
                name={participant._id}
                control={control}
                defaultValue={false}
                render={(props) => (
                  <Checkbox
                    checked={props.value}
                    onChange={(e) => props.onChange(e.currentTarget.checked)}
                  />
                )}
              />
            }
            label={participant.name}
          />
        ))}
      </FormGroup>
    </FormLayout>
  );
};

export default ParticipantChooser;
