import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import React from "react";
import { Route, Switch, useParams, useRouteMatch } from "react-router-dom";
import ExpensePage from "/imports/ui/pages/ExpenseLists/ExpensePages";
import ExpensesLists from "/imports/api/expenselists";
import Expenses from "/imports/api/expenses";
import useAccount from "/imports/hooks/useAccount";
import Loading from "/imports/ui/components/Loading";
import MasterLayout from "/imports/ui/layouts/MasterLayout";
import ExpenseListPage from "/imports/ui/pages/ExpenseLists/ExpenseListPage";
import NewExpenseForm from "/imports/ui/pages/ExpenseLists/NewExpenseForm";
import ParticipantChooser from "/imports/ui/pages/ExpenseLists/ParticipantChooser";

type Participant = {
  userId?: string;
  name: string;
};

const ExpenseListPages = () => {
  const { expenseListId } = useParams<{ expenseListId: string }>();
  const { user } = useAccount();
  const match = useRouteMatch();
  const { expenses, expenseList, ready } = useTracker(() => {
    const sub = Meteor.subscribe("expenses", expenseListId);
    const subReady = sub.ready();
    const list = ExpensesLists.findOne(expenseListId);

    if (!subReady) {
      return { ready: subReady };
    }

    if (list.participants().find((p: Participant) => p.userId === user?._id)) {
      return {
        ready: subReady,
        expenses: Expenses.find().fetch(),
        expenseList: list,
      };
    }
    return {
      ready: subReady,
      expenseList: list,
      expenses: [],
    };
  }, []);

  if (!ready) {
    return (
      <MasterLayout
        appBarContent={
          <Toolbar>
            <Typography variant="h6">MoneyTrack</Typography>
          </Toolbar>
        }
      >
        <Loading />
      </MasterLayout>
    );
  }

  const userIsParticipant = expenseList
    .participants()
    .find((p: Participant) => p.userId === user?._id);

  if (!userIsParticipant) {
    const assignedParticipants = expenseList
      .participants()
      .map((p: Participant) => p.userId)
      .filter((id: string) => id != null);
    if (assignedParticipants.length === expenseList.participants().length) {
      return <p>error</p>;
    }

    return (
      <ParticipantChooser
        listId={expenseList._id}
        participants={expenseList
          .participants()
          .filter((p: Participant) => p.userId == null)}
      />
    );
  }

  return (
    <Switch>
      <Route path={`${match.url}/expense/:expenseId`}>
        <ExpensePage />
      </Route>
      <Route path={`${match.url}/add-expense`}>
        <NewExpenseForm expenseList={expenseList} />
      </Route>
      <Route path={`${match.url}`}>
        <ExpenseListPage
          expenses={expenses}
          expenseList={expenseList}
          transfers={expenseList.transfers()}
          balances={expenseList.balances()}
          participants={expenseList.participants()}
        />
      </Route>
    </Switch>
  );
  // <ExpenseListLayout title={expenseList?.title} />;
};

export default ExpenseListPages;
