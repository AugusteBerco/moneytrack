/* eslint-disable react/jsx-props-no-spreading */
import { Tab, Tabs } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ListAltIcon from "@material-ui/icons/ListAlt";
import MoreVert from "@material-ui/icons/MoreVert";
import SwapHorizIcon from "@material-ui/icons/SwapHoriz";
import React, { useState } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import SwipeableViews from "react-swipeable-views";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  Balance,
  Expense,
  ExpenseList,
  Participant,
  Transfer,
} from "/imports/typings";
import ExpensesListFooter from "/imports/ui/components/ExpenseListFooter";
import ExpensesList from "/imports/ui/components/ExpensesList";
import MasterLayout from "/imports/ui/layouts/MasterLayout";
import BalancesTabContent from "/imports/ui/components/BalancesTabContent";

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));

function a11yProps(index: any) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

interface IProps {
  expenses?: Expense[] | undefined;
  expenseList: ExpenseList;
  transfers: Transfer[];
  balances: Balance[];
  participants: Participant[];
}
const ExpenseListPage: React.FC<IProps> = ({
  expenseList,
  expenses,
  transfers,
  balances,
  participants,
}) => {
  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme();
  const match = useRouteMatch();

  const [value, setValue] = useState(0);
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);

  const handleMenuClick: (e: React.MouseEvent) => void = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleMenuClose = () => setAnchorEl(null);

  const handleBack = () => history.push("/expenses");
  const handleChangeIndex = (index: number) => setValue(index);
  const handleTabChange = (_: React.ChangeEvent<{}>, val: number) => {
    setValue(val);
  };
  const handleShare = async () => {
    try {
      await navigator.clipboard.writeText(match.url);
      toast.info("Copied link to clipboard!");
      handleMenuClose();
    } catch (e) {
      toast.error("An error occurred");
    }
  };

  return (
    <MasterLayout
      appBarContent={
        <>
          <Toolbar>
            <IconButton onClick={handleBack} color="inherit">
              <ArrowBackIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {expenseList.title}
            </Typography>
            <IconButton
              aria-controls="user-menu"
              aria-haspopup="true"
              onClick={handleMenuClick}
              color="inherit"
            >
              <MoreVert />
            </IconButton>
            <Menu
              id="user-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleMenuClose}
            >
              <MenuItem onClick={handleShare}>Share</MenuItem>
            </Menu>
          </Toolbar>
          <Tabs
            value={value}
            onChange={handleTabChange}
            variant="fullWidth"
            indicatorColor="primary"
            textColor="primary"
          >
            <Tab label="Expenses" icon={<ListAltIcon />} {...a11yProps(0)} />
            <Tab label="Balances" icon={<SwapHorizIcon />} {...a11yProps(1)} />
          </Tabs>
        </>
      }
    >
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
        animateHeight
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <ExpensesList
            expenseList={expenseList}
            expenses={expenses}
            participants={participants}
          />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <BalancesTabContent balances={balances} transfers={transfers} />
        </TabPanel>
      </SwipeableViews>
      {value === 0 ? <ExpensesListFooter expenseList={expenseList} /> : null}
    </MasterLayout>
  );
};

export default ExpenseListPage;
