import { yupResolver } from "@hookform/resolvers/yup";
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  MenuItem,
  TextField,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Meteor } from "meteor/meteor";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import NumberFormat from "react-number-format";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import * as yup from "yup";
import { EXPENSE_CATEGORIES } from "/imports/api/constants";
import { createExpense } from "/imports/api/methods";
import useAccount from "/imports/hooks/useAccount";
import FormLayout from "/imports/ui/layouts/FormLayout";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
  },
}));

const ExpenseSchema = yup.object().shape({
  title: yup.string().required(),
  category: yup.string(),
  amount: yup.string().required(),
});

interface IProps {
  expenseList: any;
}

const NewExpenseForm: React.FC<IProps> = ({ expenseList }) => {
  const classes = useStyles();
  const history = useHistory();
  const { user } = useAccount();
  console.log({ user });
  const { register, handleSubmit, control, errors } = useForm({
    defaultValues: {
      title: "",
      category: "unset",
      amount: "",
      paidBy: user?._id,
    },
    resolver: yupResolver(ExpenseSchema),
  });
  const saveExpense = (d) => {
    console.log("saving");
    console.log(d);
    createExpense.call(
      {
        listId: expenseList._id,
        title: d.title,
        category: d.category,
        amount: d.amount,
        paidBy: d.paidBy,
        forWhom: Object.values(d.forWhom).filter((fw) => fw),
      },
      (err: Meteor.Error) => {
        if (err) {
          console.log(err);
          toast.error(err.reason);
        }
        history.push(`/expenses/${expenseList._id}/list`);
      }
    );
  };
  return (
    <FormLayout title="New Expense" handleCheck={handleSubmit(saveExpense)}>
      <div className={classes.root}>
        <TextField
          fullWidth
          name="title"
          inputRef={register}
          label="Title"
          error={!!errors.title}
          helperText={errors.title?.message}
        />
        <Controller
          name="category"
          control={control}
          render={({ onChange }) => (
            <TextField
              id="category-input"
              select
              label="Category"
              defaultValue="unset"
              onChange={onChange}
            >
              {EXPENSE_CATEGORIES.map(
                (option: { value: string; label: string }) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                )
              )}
            </TextField>
          )}
        />
        <Controller
          name="amount"
          control={control}
          defaultValue="unset"
          render={({ onChange }) => (
            <NumberFormat
              label="Amount"
              inputMode="numeric"
              onValueChange={(value) => onChange(value.floatValue)}
              customInput={TextField}
              thousandSeparator
              isNumericString
              suffix="€"
              decimalScale={2}
              fixedDecimalScale
              error={!!errors.amount}
              helperText={errors.amount?.message}
            />
          )}
        />
        <Controller
          name="paidBy"
          control={control}
          defaultValue={user?._id}
          render={({ onChange }) => (
            <TextField
              id="paid-input"
              select
              defaultValue={user?._id}
              label="Paid by"
              onChange={onChange}
            >
              {expenseList
                .participants()
                .map(
                  (participant: {
                    _id: string;
                    name: string;
                    userId?: string;
                  }) => (
                    <MenuItem key={participant._id} value={participant._id}>
                      {participant.name}
                    </MenuItem>
                  )
                )}
            </TextField>
          )}
        />
        <FormControl component="fieldset">
          <FormLabel component="legend">FOR WHOM</FormLabel>
          <FormGroup>
            {expenseList
              .participants()
              .map(
                (participant: {
                  _id: string;
                  name: string;
                  userId?: string;
                }) => (
                  <FormControlLabel
                    key={participant._id}
                    value={participant._id}
                    control={<Checkbox />}
                    label={participant.name}
                    name={`forWhom[${participant._id}]`}
                    inputRef={register}
                  />
                )
              )}
          </FormGroup>
        </FormControl>
      </div>
    </FormLayout>
  );
};

export default NewExpenseForm;
