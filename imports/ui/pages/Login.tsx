import React from "react";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { makeStyles } from "@material-ui/core";
import { Meteor } from "meteor/meteor";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import Layout from "/imports/ui/layouts/Layout";

interface LoginFormInputs {
  email: string;
  password: string;
}
const LoginFormSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required(),
});

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LoginPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const handleLogin = async (d: LoginFormInputs) => {
    Meteor.loginWithPassword(
      { email: d.email },
      d.password,
      (error?: Error | Meteor.Error | Meteor.TypedError | undefined) => {
        if (error) {
          if (error instanceof Meteor.Error) {
            toast.error(error.reason);
          } else {
            toast.error(error.message);
          }
        } else {
          history.push("/");
        }
      }
    );
  };

  const { register, handleSubmit, errors } = useForm<LoginFormInputs>({
    resolver: yupResolver(LoginFormSchema),
  });

  return (
    <Layout>
      <div className={classes.paper}>
        <Typography variant="h2">Sign In</Typography>

        <form
          className={classes.form}
          onSubmit={handleSubmit(handleLogin)}
          noValidate
        >
          <TextField
            name="email"
            inputRef={register}
            error={!!errors.email}
            helperText={errors.email?.message}
            required
            fullWidth
            id="email"
            label="Email Address"
            autoComplete="email"
            autoFocus
          />
          <TextField
            type="password"
            name="password"
            inputRef={register}
            error={!!errors.password}
            helperText={errors.password?.message}
            id="password"
            label="Password"
            autoComplete="current-password"
            required
            fullWidth
          />
          <Button
            type="submit"
            className={classes.submit}
            fullWidth
            variant="contained"
            color="primary"
          >
            Login
          </Button>
        </form>
      </div>
    </Layout>
  );
};

export default LoginPage;
