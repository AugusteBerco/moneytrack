import React from "react";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { makeStyles } from "@material-ui/core";
import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

const RegistrationFormSchema = yup.object().shape({
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().required(),
  confirmPassword: yup
    .string()
    .required()
    .oneOf([yup.ref("password"), null], "Passwords don't match"),
});

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
interface RegisterFormInputs {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
}
const RegisterPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm<RegisterFormInputs>({
    resolver: yupResolver(RegistrationFormSchema),
  });

  const handleRegistration = async (d: RegisterFormInputs) => {
    Accounts.createUser(
      {
        username: d.username,
        email: d.email,
        password: d.password,
      },
      (error?: Error | Meteor.Error | Meteor.TypedError | undefined) => {
        if (error) {
          if (error instanceof Meteor.Error) {
            toast.error(error.reason);
          } else {
            toast.error(error.message);
          }
        } else {
          history.push("/");
        }
      }
    );
  };
  return (
    <div className={classes.paper}>
      <Typography variant="h2">Sign In</Typography>

      <form
        className={classes.form}
        onSubmit={handleSubmit(handleRegistration)}
        noValidate
      >
        <TextField
          name="username"
          inputRef={register}
          error={!!errors.username}
          helperText={errors.username?.message}
          required
          fullWidth
          id="username"
          label="User Name"
          autoComplete="username"
          autoFocus
        />
        <TextField
          name="email"
          inputRef={register}
          error={!!errors.email}
          helperText={errors.email?.message}
          required
          fullWidth
          id="email"
          label="Email Address"
          autoComplete="email"
        />
        <TextField
          type="password"
          name="password"
          inputRef={register}
          error={!!errors.password}
          helperText={errors.password?.message}
          id="password"
          label="Password"
          autoComplete="current-password"
          required
          fullWidth
        />
        <TextField
          type="password"
          name="confirmPassword"
          inputRef={register}
          error={!!errors.confirmPassword}
          helperText={errors.confirmPassword?.message}
          id="password"
          label="Confirm Password"
          required
          fullWidth
        />
        <Button
          type="submit"
          className={classes.submit}
          fullWidth
          variant="contained"
          color="primary"
        >
          Register
        </Button>
      </form>
    </div>
  );
};

export default RegisterPage;
