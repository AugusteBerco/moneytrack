import { Mongo } from "meteor/mongo";
import SimpleSchema from "simpl-schema";
import currency from "currency.js";
import Expenses from "/imports/api/expenses";
import Participants from "/imports/api/participants";

const ExpenseLists = new Mongo.Collection("expenselists");

ExpenseLists.schema = new SimpleSchema({
  title: { type: String },
});

ExpenseLists.attachSchema(ExpenseLists.schema);

ExpenseLists.helpers({
  expenses() {
    return Expenses.find({ listId: this._id }).fetch();
  },
  participants() {
    return Participants.find({ listId: this._id }).fetch();
  },
  totalExpenses() {
    return this.expenses().reduce(
      (total, expense) => total.add(expense.amount),
      currency(0)
    );
  },
  totalFor(userId) {
    const expenses = this.expenses();
    const participant = Participants.findOne({ listId: this._id, userId });
    const participantId = participant._id;
    const total = expenses.reduce(
      (t, expense) =>
        expense.forWhom.includes(participantId)
          ? t.add(currency(expense.amount).divide(expense.forWhom.length))
          : t,
      currency(0)
    );
    return total;
  },
  balances() {
    const participants = Participants.find({ listId: this._id }).fetch();
    const expenses = this.expenses();
    const whoOwns = {};
    participants.forEach((p) => {
      whoOwns[p._id] = {
        name: p.name,
        total: currency(0, { symbol: "€" }),
      };
    });
    expenses.forEach((expense) => {
      participants.forEach((p) => {
        if (p._id === expense.paidBy) {
          whoOwns[p._id].total = expense.forWhom.includes(p._id)
            ? whoOwns[p._id].total
                .add(currency(expense.amount))
                .subtract(
                  currency(expense.amount).divide(expense.forWhom.length)
                )
            : whoOwns[p._id].total.add(currency(expense.amount));
        } else {
          whoOwns[p._id].total = expense.forWhom.includes(p._id)
            ? whoOwns[p._id].total.subtract(
                currency(expense.amount).divide(expense.forWhom.length)
              )
            : whoOwns[p._id].total;
        }
      });
    });

    return Object.values(whoOwns).sort((a, b) => a.total.value - b.total.value);
  },
  transfers() {
    let balances = this.balances();

    const transfers = [];
    while (balances.length !== 0) {
      const sortedBalances = balances.sort(
        (a, b) => a.total.value - b.total.value
      );

      const mostOwns = sortedBalances[0];

      let closestBalance = mostOwns.total.value;
      const suitableTransfer = sortedBalances.reduce((res, balance) => {
        if (
          Math.abs(mostOwns.total.value + balance.total.value) <=
          Math.abs(closestBalance)
        ) {
          closestBalance = res.total.value + balance.total.value;

          return balance;
        }
        return res;
      }, mostOwns);
      transfers.push({
        name: mostOwns.name,
        to: suitableTransfer.name,
        amount:
          suitableTransfer.total.value >= Math.abs(mostOwns.total.value)
            ? currency(Math.abs(mostOwns.total.value), { symbol: "€" })
            : currency(suitableTransfer.total.value, { symbol: "€" }),
      });

      const mostOwnsCopy = currency(mostOwns.total.value);

      sortedBalances[0].total =
        suitableTransfer.total.value >= Math.abs(mostOwns.total.value)
          ? currency(0)
          : mostOwns.total.add(suitableTransfer.total);

      suitableTransfer.total =
        suitableTransfer.total.value > Math.abs(mostOwnsCopy.value)
          ? suitableTransfer.total.add(mostOwnsCopy)
          : currency(0);

      balances = sortedBalances.filter((b) => b.total.value !== 0);
    }
    return transfers;
  },
});

export default ExpenseLists;
