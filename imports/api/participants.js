import { Mongo } from "meteor/mongo";
import SimpleSchema from "simpl-schema";

const Participants = new Mongo.Collection("participants");

Participants.schema = new SimpleSchema({
  listId: String,
  name: { type: String },
  userId: { type: String, optional: true },
});

Participants.attachSchema(Participants.schema);

export default Participants;
