import { ValidatedMethod } from "meteor/mdg:validated-method";
import { Meteor } from "meteor/meteor";
import SimpleSchema from "simpl-schema";
import ExpenseLists from "/imports/api/expenselists";
import Expenses from "/imports/api/expenses";
import Participants from "/imports/api/participants";

const checkUserCanAddExpenseToList = ({ userId, expenseList }) => {
  const userIsParticipant = expenseList
    .participants()
    .find((p) => p.userId === userId);
  if (!userIsParticipant) {
    throw new Meteor.Error(
      "expenselist.unauthorized",
      "User is not participant"
    );
  }
};

const checkUserIsLoggedIn = ({ methodName, userId }) => {
  const currentUser = Meteor.users.findOne(userId);

  if (!currentUser) {
    throw new Meteor.Error(
      `${methodName}.unauthorized`,
      "Current user is anonymous"
    );
  }
  return currentUser;
};

const checkListExists = ({ methodName, listId }) => {
  const list = ExpenseLists.findOne(listId);
  if (!list) {
    throw new Meteor.Error(
      `${methodName}.notFound`,
      "The expense list was not found."
    );
  }
  return list;
};

export const createExpenseList = new ValidatedMethod({
  name: "createList",
  validate: new SimpleSchema({
    title: String,
    others: Array,
    "others.$": String,
  }).validator(),
  run({ title, others }) {
    const currentUser = checkUserIsLoggedIn({
      methodName: "createList",
      userId: this.userId,
    });

    const expenseListId = ExpenseLists.insert({
      title,
    });

    Participants.insert({
      listId: expenseListId,
      userId: this.userId,
      name: currentUser.username,
    });
    others.map((o) => Participants.insert({ listId: expenseListId, name: o }));
  },
});

export const assignToList = new ValidatedMethod({
  name: "assignToList",
  validate: new SimpleSchema({
    participantId: String,
  }).validator(),
  run({ participantId }) {
    checkUserIsLoggedIn({ methodName: "assignToList", userId: this.userId });

    const participantToUpdate = Participants.findOne({ _id: participantId });

    if (!participantToUpdate) {
      throw new Meteor.Error(
        "assignToList.invalid",
        "The list does not have this participant."
      );
    }

    if (participantToUpdate.userId) {
      throw new Meteor.Error(
        "assignToList.invalid",
        "The participant is already assigned."
      );
    }

    Participants.update(
      { _id: participantToUpdate._id },
      { $set: { userId: this.userId } }
    );
  },
});

export const createExpense = new ValidatedMethod({
  name: "createExpense",
  validate: new SimpleSchema({
    listId: String,
    title: String,
    amount: String,
    category: {
      type: String,
      optional: true,
    },
    paidBy: String,
    forWhom: Array,
    "forWhom.$": String,
  }).validator(),
  run(expenseInput) {
    const currentUser = checkUserIsLoggedIn({
      methodName: "createExpense",
      userId: this.userId,
    });
    const { listId, title, amount, category, paidBy, forWhom } = expenseInput;

    const listToUpdate = checkListExists({
      methodName: "createExpense",
      listId,
    });

    checkUserCanAddExpenseToList({
      userId: currentUser._id,
      expenseList: listToUpdate,
    });

    Expenses.insert({
      listId,
      title,
      amount,
      category,
      paidBy,
      forWhom,
      date: new Date(),
    });
  },
});
export default {};
