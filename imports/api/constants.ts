export const EXPENSE_CATEGORIES = [
  {
    value: "unset",
    label: "No Category",
  },
  {
    value: "rent",
    label: "Rent & Charges",
  },
  {
    value: "accomodation",
    label: "Accomodation",
  },
  {
    value: "fun",
    label: "Entertainment",
  },
  {
    value: "groceries",
    label: "Groceries",
  },
  {
    value: "health",
    label: "Healthcare",
  },
  {
    value: "insurance",
    label: "Insurance",
  },
  {
    value: "restaurants",
    label: "Restaurant & Bars",
  },
  {
    value: "shopping",
    label: "Shopping",
  },
  {
    value: "transport",
    label: "Transport",
  },
];
export default {};
