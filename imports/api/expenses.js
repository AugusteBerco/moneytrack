import { Mongo } from "meteor/mongo";
import SimpleSchema from "simpl-schema";

const Expenses = new Mongo.Collection("expenses");

Expenses.schema = new SimpleSchema({
  listId: {
    type: String,
  },
  title: {
    type: String,
    max: 50,
  },
  expenseType: { type: String, defaultValue: "expense" },
  tag: { type: String, optional: true },
  amount: { type: Number },
  date: { type: Date },
  paidBy: { type: String },
  forWhom: {
    type: Array,
  },
  "forWhom.$": {
    type: String,
  },
});

Expenses.attachSchema(Expenses.schema);
// Expenses.helpers({
//   list() {
//     return ExpenseLists.findOne(this.listId);
//   },
// });

export default Expenses;
