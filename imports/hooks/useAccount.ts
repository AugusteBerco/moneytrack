import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";

const useAccount = () =>
  useTracker(() => {
    const user = Meteor.user();
    const loggingIn = Meteor.loggingIn();
    return {
      user,
      userId: user?._id,
      loggingIn,
      isLoggedIn: !!user,
    };
  }, []);

export default useAccount;
