import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import ExpenseLists from "/imports/api/expenselists";

const useExpenseList = (expenseListId: string) =>
  useTracker(() => {
    const sub = Meteor.subscribe("expenses", expenseListId);

    return {
      loading: !sub.ready(),
      expenseList: ExpenseLists.findOne(expenseListId),
    };
  }, [expenseListId]);

export default useExpenseList;
